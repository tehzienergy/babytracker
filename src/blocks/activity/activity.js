// Temporary scripts

$('.activity--feeding .tracker__breast-button').click(function(e) {
  e.preventDefault();
  $('.activity').addClass('activity--breastfeeding-started');
  $('.tracker__breast-button').removeClass('tracker__breast-button--active');
  $(this).addClass('tracker__breast-button--active');
});

$('.activity--feeding .button--finish').click(function(e) {
  e.preventDefault();
  $('.activity').removeClass('activity--breastfeeding');
  $('.activity').addClass('activity--breastfeeding-result');
  $('.tracker').addClass('tracker--short');
});

$('.activity--feeding .activity__manual').click(function(e) {
  e.preventDefault();
  $('.tracker').addClass('tracker--short');
  $('.activity').addClass('activity--breastfeeding-manual');
});

$('.activity--feeding .tracker__button-bar .button-bar__item:last-child').click(function() {
  $('.activity').addClass('activity--bottle');
});

$('.activity--feeding .tracker__button-bar .button-bar__item:first-child').click(function() {
  $('.activity').removeClass('activity--bottle');
});

$('.activity--feeding .activity__controls-bottle-initial .button').click(function(e) {
  e.preventDefault();
  $('.activity').addClass('activity--bottle-history');
});

$('.activity--sleep .button').click(function(e) {
  e.preventDefault();
  $('.activity').addClass('activity--sleep-in-progress');
});

$('.activity--sleep .button--finish').click(function(e) {
  e.preventDefault();
  $('.activity').removeClass('activity--sleep-in-progress');
  $('.activity').addClass('activity--sleep-finished');
});

$('.activity--sleep .activity__manual').click(function(e) {
  e.preventDefault();
  $('.activity').addClass('activity--sleep-manual');
});