$('.range').rangeslider({
  polyfill: false,
  onSlide: function(position, value) {
    $('.tracker__bottle-amount-number').html(value);
  },
});